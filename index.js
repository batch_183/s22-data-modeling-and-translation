
//USERS
{
"_id":"user001",
"firstName": "Juan",
"lastName" : "DelaCruz",
"email" : "jdelacruz@gmail.com",
"password" : "jdelacruz123",
"isAdmin" : false,
"mobileNumber" : "09123456789",

}

{
    
"_id":"user002",
"firstName": "Jane",
"lastName" : "Roque",
"email" : "janeroque@mail.com",
"password" : "janeroque123",
"isAdmin" : false,
"mobileNumber" : "09123123123",
}


// ORDERS

{

"_id": "order00001",
"transactionDate" : "2022-06-10T15:00:00.00Z",
"status": "shipped",
"Total": 15 
}

{

"_id": "order00002",
"transactionDate" : "2022-015-10T15:00:00.00Z",
"status": "ordered",
"Total": 5
}

// PRODUCTS

{

"productName":"Socks",
"Description":"large, color blue, puma",
"price": 600,
"stocks": 500,
"isActive": true,
"sku": "020000SLR"

}

{

"productName":"Shoes",
"Description":"22, nike, Air Jordan",
"price": 8000,
"stocks": 25,
"isActive": true,
"sku": "0200321SLR"
    
}
   
// ORDER PRODUCTS

{
"orderID":"OrderID00001",
"productID":"PID000001",
"quantity":2,
"price":8600,
"subTotal":8600

}